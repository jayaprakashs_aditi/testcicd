using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestBitBucket
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        [TestCategory("DEMO")]
        public void TestMethod1()
        {
            System.Console.WriteLine("this is first test");
            Assert.AreEqual("AB", "BA");
        }

        [TestMethod]
         [TestCategory("UAT")]
        public void TestMethod2()
        {
            Assert.AreEqual("AB", "BA");
        }



        [TestMethod]
         [TestCategory("UAT")]
        public void TestMethod3()
        {
            Assert.AreEqual("AB", "aB");
        }
    }
}
